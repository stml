/* stml.h
* Status Table Modification Language - Main Library
* Written by Michael D. Reiley (Seisatsu)
* Copyright (c) 2010, Omega Software Development Group
* Released under ISC license
*/

#ifndef STML_H
#define STML_H

#include <stdio.h>
#include "bool.h" /*Define the Boolean Datatype*/

/* -- DEFINITIONS SECTION -- */

enum STML_STATE_TYPE { /*Types of states*/
	parent_data_t, /*Fake data type that points to one or more substates*/
	bool_data_t, /*Boolean type*/
	int_data_t, /*Integer type*/
	string_data_t, /*String type*/
	command_data_t /*A char type whose contents disappear immediately after reading*/
};

typedef struct STML_STATE__ {
	char* id; /*Identifier of state*/
	unsigned long cbid; /*Callback ID of state; will always be unique*/
	STML_STATE_TYPE type; /*Type of state*/
	struct STML_STATE__** parent_data; /*State contains more states*/
	bool* bool_data; /*State contains a boolean*/
	int* int_data; /*State contains an integer*/
	char** string_data; /*State contains a string*/
	char** command_data; /*State contains a command*/
} STML_STATE;

typedef struct {
	char* id; /*Identifier of object*/
	STML_STATE** states; /*Child states*/
} STML_OBJECT;

typedef struct { /*This is the only struct that the end user needs to care about.*/
	char* id; /*Identifier of table*/
	STML_OBJECT** objects; /*Child objects*/
} STML_TABLE;

/* -- FUNCTION SECTION -- */

/* (*stml_callback)
* Function pointer to callback the parent program when a change is made.
* Programs can pass a custom callback function.
*
* Arguments:
* -- 1) STML_TABLE* : STML Table to be used
* -- 2) int : Callback ID for the modified state. See the manual.
* -- 2) void* : Convenience variable that immediately returns the new state.
*/
typedef void (*stml_callback) (STML_TABLE* table, unsigned long cbid, void* state);

/* stml_create_table
* Create a new STML table.
* An empty table is returned.
*
* Arguments:
* -- NONE
*/
STML_TABLE* stml_create_table ();

/* stml_destroy_table
* Destroy the table passed to this function.
*
* Arguments:
* -- STML_TABLE* : The table to be destroyed.
*/
void stml_destroy_table (STML_TABLE* table);

/* stml_command
* Pass a command to the interpreter.
* Returns 0 if successful, error code otherwise.
*
* Arguments:
* -- 1) STML_TABLE* : The table to be used.
* -- 2) char* : Command string to be interpreted.
* -- 3) void* : Returns data if the command is a query type.
* -- 4) int* : If a new state was created, returns a unique callback ID. Otherwise, 0. See the manual.
*/
int stml_command (STML_TABLE* table, char* command, void* data, int* cbid);

/* stml_save
* Save a table in XML format.
*
* Arguments:
* -- 1) STML_TABLE* : Table to save.
* -- 2) FILE* : File pointer to dump XML data to.
*/
void stml_save (STML_TABLE* table, FILE* file);

/* stml_load
* Load a table in XML format.
* Returns a table if successful, null if failed.
*
* Arguments:
* -- 1) FILE* : File pointer to load XML data from.
*/
STML_TABLE* stml_load (STML_TABLE* table, FILE* file);

/* stml_macro_import
* Import a macro definition table into the interpreter.
* Returns 0 if successful, error code otherwise.
* Requires STML_XML extension.
*
* Arguments:
* -- 1) FILE* : File pointer to definition table XML file.
*/
int stml_macro_import (FILE* macro_table);

#endif
